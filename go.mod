module codeberg.org/gruf/go-hashenc

go 1.16

require (
	codeberg.org/gruf/go-bytes v1.0.0
	github.com/zeebo/blake3 v0.2.1 // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
)
